package SoSo;

use 5.018004;
use strict;
use warnings;
use XML::LibXML;
use Data::Dumper;
use List::Util qw(min max);

our $VERSION = '0.01';

my $origin_element = '//a[@id="make-everything-ok-button"]';
my $threshold = 0.5;

sub run {
    my $origin = $ARGV[0] or die "No origin file argument found";
    my $target = $ARGV[1] or die "No target file argument found";

    my $soso = SoSo->new($origin);
    my $found = $soso->find($target);
    if ($found) {
        print "Found element\n";
        print $found->{node}->nodePath() . "\n";
        _pretty_print_node($found->{node}) if $ENV{DEBUG};
    }
    else {
        die "Failed to find any similar element, sorry :(";
    }
}

sub new {
    my ($class, $origin_file) = @_;

    my $self = {};

    my $attrs = {};
    $self->{parser} = XML::LibXML->new;

    my $dom = $self->{parser}->parse_file($origin_file);

    my @nodes = $dom->findnodes($origin_element);
    if (scalar @nodes != 1) {
        die "The original element should be exactly one";
    }
    my $node = $nodes[0];
    $attrs = _fetch_attrs($node);
    $self->{origin_node} = $node->nodeName();

    $self->{attrs} = $attrs;
    return bless $self, $class;
}

sub parser {
    shift->{parser};
    # todo mooose fancy oop
}


sub find {
    my ($self, $file) = @_;

    my $dom = $self->parser->parse_file($file);

    my $node_name = $self->{origin_node};
    my @initial_list = $dom->findnodes("//$node_name");

    # This function is supposed to return similarity of definitely not equal strings
    my $scorer = sub {
        my ($a1, $a2);

        # jaro winkler distance was planned to be here but for some reason xs
        # module fails to install on catalina and I don't have time to debug it
        # surprise, it works well without it at least on the test set
        # die Dumper [_jaro($a1, $a2), $a1, $a2];
        return _jaro($a1, $a2);
        return 0.5;
    };
    my $candidates = $self->_filter($scorer, \@initial_list);

    if (scalar @$candidates > 0) {
        # I would like to apply anothe distance here to further
        # refine the set but not today
    }

    if (scalar @$candidates == 1) {
        # eee-haa, found ya
        return $candidates->[0];
    }

    my $max = 0;

    my $retval;
    for (@$candidates) {
        _pretty_print_node($_->{node});
        _debug($_->{score});
        if ($_->{score} > $max) {
            $retval = $_;
            $max = $_->{score};
        }
    }
    return $retval;
}


sub _filter {
    my ($self, $function, $candidates) = @_;

    my @retval = ();
    for my $c (@$candidates) {
        my $score = $self->_compare($c, $function);
        if ($score > $threshold) {
            push @retval, { node => $c, score => $score };
        }
    }
    return \@retval;
}

sub _pretty_print_node {
    my ($node) = @_;

    print $node->nodePath() . "\n";
    for my $a ($node->getAttributes()) {
        print $a->getName() . ' -> ' . $a->getValue() . "\n";
    }
    print $node->textContent() . "\n";
}


sub _fetch_attrs {
    my ($node) = @_;

    my $attrs = {};
    for my $attr ($node->getAttributes()) {
        my $name = $attr->getName();
        $attrs->{$name} = $attr->getValue();
        # TODO check hierarchy???
    }

    $attrs->{_content} = $node->textContent();
    return $attrs;
}

sub _compare {
    my ($self, $node, $func) = @_;

    my $attrs = _fetch_attrs($node);
    # score 0 .. 1
    # 0 - no match
    # 1 - complete match
    # total score = avg 0 .. 1
    my $scores = {};

    # too lazy to find a proper list::util for avg
    my $total = 0 ;
    for my $name (keys %{$self->{attrs}}) {
        my $value = $self->{attrs}->{$name};
        $scores->{$name} = _score($func, $value, $attrs->{$name});
        $total += $scores->{$name};
    }

    _debug($scores);
    my $avg = $total / scalar keys %{$self->{attrs}};
    return $avg;
}

sub _score {
    my ($func, $a1, $a2) = @_;

    if (!$a1 && !$a2) {
        return 1;
    }
    if (!$a1 || !$a2) {
        return 0;
    }
    if ($a1 eq $a2) {
        return 1;
    }
    else {
        return $func->($a1, $a2);
    }
}

sub _debug {
    my @messages = @_;

    unless ($ENV{DEBUG}) {
        return;
    }

    for (@messages) {
        if (ref $_) {
            print Dumper $_;
        }
        else {
            print "$_\n";
        }
    }
}

# stolen from rosetta
sub _jaro {
    my ($s, $t) = @_;

    my $s_len = length($s);
    my $t_len = length($t);

    no warnings 'uninitialized';

    return 1 if $s_len == 0 and $t_len == 0;

    my $match_distance = int(max($s_len, $t_len) / 2) - 1;

    my @s_matches;
    my @t_matches;

    my @s = split(//, $s);
    my @t = split(//, $t);

    my $matches = 0;
    foreach my $i (0 .. $#s) {

        my $start = max(0, $i - $match_distance);
        my $end = min($i + $match_distance + 1, $t_len);

        foreach my $j ($start .. $end - 1) {
            $t_matches[$j] and next;
            $s[$i] eq $t[$j] or next;
            $s_matches[$i] = 1;
            $t_matches[$j] = 1;
            $matches++;
            last;
        }
    }

    return 0 if $matches == 0;

    my $k              = 0;
    my $transpositions = 0;

    foreach my $i (0 .. $#s) {
        $s_matches[$i] or next;
        until ($t_matches[$k]) { ++$k }
        $s[$i] eq $t[$k] or ++$transpositions;
        ++$k;
    }

    (($matches / $s_len) + ($matches / $t_len) +
        (($matches - $transpositions / 2) / $matches)) / 3;
}

# turning module into a cli which is also a module hehe
run() unless caller;

1;


