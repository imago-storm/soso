# Before 'make install' is performed this script should be runnable with
# 'make test'. After 'make install' it should work as 'perl SoSo.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use strict;
use warnings;
use File::Basename qw(dirname);
use File::Spec;
use Data::Dumper;

use Test::More tests => 6;
BEGIN { use_ok('SoSo') };

my $t_dir = dirname($0);
my $origin = File::Spec->catfile($t_dir, 'sample-0-origin.html');
print "Origin file: $origin\n";

my $soso = SoSo->new($origin);

my $r1 = $soso->find(File::Spec->catfile($t_dir, 'sample-1-evil-gemini.html'));
my $xpath = $r1->{node}->nodePath();
SoSo::_pretty_print_node($r1->{node});

is($xpath, "/html/body/div/div/div[3]/div[1]/div/div[2]/a[2]");

my $r2 = $soso->find(File::Spec->catfile($t_dir, 'sample-2-container-and-clone.html'));
my $xpath = $r2->{node}->nodePath();
SoSo::_pretty_print_node($r2->{node});
print $r2->{score} . "\n";

is($xpath, "/html/body/div/div/div[3]/div[1]/div/div[2]/div/a");


my $r3 = $soso->find(File::Spec->catfile($t_dir, 'sample-3-the-escape.html'));
my $xpath = $r3->{node}->nodePath();
SoSo::_pretty_print_node($r3->{node});
print $r3->{score} . "\n";

is($xpath, "/html/body/div/div/div[3]/div[1]/div/div[3]/a");


my $r4 = $soso->find(File::Spec->catfile($t_dir, 'sample-4-the-mash.html'));
my $xpath = $r4->{node}->nodePath();
SoSo::_pretty_print_node($r4->{node});
print $r4->{score} . "\n";
is($xpath, "/html/body/div/div/div[3]/div[1]/div/div[3]/a");

my $r5 = $soso->find(File::Spec->catfile($t_dir, 'sample-5-nothing.html'));
is($r5, undef);

